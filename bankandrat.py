import logging
import requests
import json
from elasticsearch import Elasticsearch
session = requests.Session()

session.headers.update({
"Accept": "application/json",
"Content-Type":"application/json"
})


#mapping the elkstack, make changes here if the value changes
es = Elasticsearch(
    ['neo-3.ccirc.geekweek'],
    scheme="http",
    port=9200,
)
#Creates a list of regex quiries below
#Please add below when there is a new new company/bank name that needs to be added to moniter
#the regex below is looking for the following malicious format http://randomdomai.tld/random/legitbankname/random
regexquery=[".*(atb|bmo|cibc|desj|desjardins|hsbc|laurentian|lloydsbank|manulife|meridian|mbna|nationalbank|bnc|rbc|rbc2|\/rbs|\/sco|scotia|scoatiabank|simpli|simplii|tangerine|\/td|wellsfargo|test)($|\/.*)",
"^(http(s)?(:\/\/))?(www.)[a-zA-Z0-9-_.\/](accweb.mouv.desjardins.com|banco.brasil.com.br|chaseonline.chase.com|chaseonline.com|easyweb.td.com|hellobank.fr|login.bankofamerica.com|m.wellsfargo.com|rbcroyalbank.com|secure.bankofamerica.com|secure.ingdirect.fr|secure.tangerine.ca|tdbank.com|wellsfargo.com|www.atbonline.com|www.bmo.com|www.cibc.com|www.cibc.mobi|www.credit|agricole.fr|www.rbs.co.uk|www.scotiaonline.scotiabank.com|www.security.hsbc.co.uk|www1.royalbank.com|www2.soctiaonline.scotiabank.com)($|\/.*)"]
#The regex query above for the following format http://randomdomain.tld/random/secure.bankname.com/random

#Create a for loop to go through the list of bank related regex
for x in regexquery:
	print(x)
	res = es.search(index="c2_v3", doc_type="panels", body={
	"size": 100,
	"query": {
    "regexp":{
		"panel_url.keyword": {
			"value": x
		}
		}
		}
		}
	)
	print("%d documents found" % res['hits']['total'])
	print(json.dumps(res, indent=2))

#Creating a dictionary of rat names and regexquiries to detect it
ratregex={
"gh0strat": r"(Gh0st\\|PCRat_|HeiSe\\336|cb1st.|cb1st;\\002|cb1st@\\002|cb1st\\003\\002|cb1st\\346\\001|d\\001|Heart\\360\\001|Heart\\357\\001|Hyxhj\\254\\001|KrisR\\310\\)(.*)",
"H-worm": r"GET \/.*.functions HTTP\/1.1",
"Xtreme RAT": r"POST \/is-ready HTTP\/1.1.*",
"ISRStealer": r"GET .*\.php\?action=.*&username=.*&password=.*&app=.*&pcname=.*&sitename=.* HTTP/1\.1.*User-Agent: HardCore Software.*",
"DiamondFox": r"(GET .*/plugins/.*\.p HTTP/1\.1.*)",
"DiamondFox": r"(POST .*post.php\?pl=&slots=1 HTTP/1.1.*)",
"TreasureHunt": r"(POST .*gate.php\?request=true HTTP/1.1.*request=.*&use=.*&id=.*)",
"Lokibot": r"(POST \/.*HTTP\/1\.0.*4\.08 \(Charon; Inferno\).*Host:.*Content-Key: [0-9A-Z]{8} .*)",
"Godzilla": r"(GET .*\.php\?g=.*&k=.* HTTP/1.1.*)",
"Danabot": r"(GET \/index\.php\?m=T&a=.*)",
"KPot": r"(POST /regbot\.php HTTP/1.1.*bot_id=.*&is_admin=[0-1]&IL=.*)",
"Predator": r"(POST /api/gate.get\?p1=.*&p2=.*&p3=.*)",
"Arkei": r"(POST /server/grubConfig HTTP/1.1)",
"Eredel": r"(POST /gate.php\?hwid=[0-9A-Z]{8}&amp;os=.*&amp;file=.*&amp;cookie)",
"Gazorp": r"(POST /[a-z0-9]{5}\.php HTTP\/1\.0.*\r\nHost:.*\r\nConnection: close\r\nContent-Length: [0-9]{3}\r\nAccept-Language: en-US\r\nContent-Type: application\/octet-stream.*$)"
}

#Forloop to go through the dictionary of items to compute and print
for rat_name,regex_rat in ratregex.items():
	print(regex_rat)
	res = es.search(index="callouts_parsed*", doc_type="doc", body={
	"size": 100,
	"query": {
    "regexp":{
		"client_payload.keyword": {
			"value": regex_rat
		}
		}
		}
		}
	)
	print("%d documents found" % res['hits']['total'])
	print("{} : {}".format(rat_name,json.dumps(res, indent=2)))
